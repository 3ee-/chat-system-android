package com.beebee.chatsystemsendbird.utils

import android.content.Context
import android.util.Log
import com.beebee.chatsystemsendbird.R

fun getUserId(activity: Context) :  String? {
    val sharedPref = activity.getSharedPreferences(activity.getString(R.string.app_name), Context.MODE_PRIVATE)
    val retrivedToken  = sharedPref.getString("USER",null)
    Log.d("Token", retrivedToken.toString())
    return retrivedToken
}

fun saveUserId(activity: Context, userId: String) {
    val sharedPref = activity.getSharedPreferences(activity.getString(R.string.app_name), Context.MODE_PRIVATE)
    sharedPref.edit().putString("USER", userId).apply()
}
package com.beebee.chatsystemsendbird.utils

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {
    val BASE_URL = "http://localhost:3000"

    fun retrofitClient(): Retrofit {
        val client = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
        return client
    }

    fun <T: Any> getService(ServiceClient: T) = retrofitClient().create(ServiceClient::class.java)
}
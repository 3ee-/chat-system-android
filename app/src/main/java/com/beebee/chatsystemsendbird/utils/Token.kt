package com.beebee.chatsystemsendbird.utils

import android.content.Context
import android.util.Log
import com.beebee.chatsystemsendbird.R

fun getToken(activity: Context) :  String? {
    val sharedPref = activity.getSharedPreferences(activity.getString(R.string.app_name), Context.MODE_PRIVATE)
    val retrivedToken  = sharedPref.getString("TOKEN",null)
    Log.d("Token", retrivedToken.toString())
    return retrivedToken
}

fun saveToken(activity: Context, token: String) {
    val sharedPref = activity.getSharedPreferences(activity.getString(R.string.app_name), Context.MODE_PRIVATE)
    sharedPref.edit().putString("TOKEN", token).apply()
}
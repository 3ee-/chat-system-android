package com.beebee.chatsystemsendbird.services.auth.login

import android.content.Context
import com.beebee.chatsystemsendbird.model.User

interface ILogin {
    fun loginService(activity: Context, user: User, callback: (String) -> Unit): Unit
}
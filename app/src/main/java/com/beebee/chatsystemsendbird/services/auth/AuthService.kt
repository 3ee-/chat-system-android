package com.beebee.chatsystemsendbird.services.auth

import com.beebee.chatsystemsendbird.model.User
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthServiceInterface {
    @POST("/auth/login")
    fun loginUser(
        @Body userData: User
    ) : Call<User>

    @POST("/auth/register")
    fun registerUser(
        @Body userData: User
    ) : Call<User>
}


object AuthService {
    fun create(): AuthServiceInterface {
        val BASE_URL = "http://192.168.0.6:5000"
        val client = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
        return client.create(AuthServiceInterface::class.java)
    }
}
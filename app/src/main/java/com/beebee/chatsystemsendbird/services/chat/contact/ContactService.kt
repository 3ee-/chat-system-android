package com.beebee.chatsystemsendbird.services.chat.contact

import android.content.Context
import android.util.Log
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.services.chat.ChatService
import com.beebee.chatsystemsendbird.utils.getToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class ContactService @Inject constructor(): IContact {
    override fun getContacts(activity: Context, callback: (List<User>) -> Unit) {
        val token = getToken(activity)
        ChatService.create(token.toString())
            .getContacts()
            .enqueue(object : Callback<List<User>> {
                override fun onFailure(call: Call<List<User>>, t: Throwable) {
                    Log.e("Failed Get contacts", t.localizedMessage)
                    callback(listOf())
                }

                override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                    Log.d("Sucessfully Get contacts", response.body().toString())
                    response.body()?.let { callback(it) }
                }
            })
    }
}
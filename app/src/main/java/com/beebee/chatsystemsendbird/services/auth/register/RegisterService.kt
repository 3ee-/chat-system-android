package com.beebee.chatsystemsendbird.services.auth.register

import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.services.auth.AuthService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class RegisterService @Inject constructor(): IRegsiter {
    override fun registerUser(user: User, callback: (String) -> Unit) {
        AuthService.create()
            .registerUser(user)
            .enqueue(object: Callback<User> {
                override fun onFailure(call: Call<User>, t: Throwable) {
                    callback("Failed registered")
                }

                override fun onResponse(call: Call<User>, response: Response<User>) {
                    callback("Successfully registered")
                }
            })
    }
}
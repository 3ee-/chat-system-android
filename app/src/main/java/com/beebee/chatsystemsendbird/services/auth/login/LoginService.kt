package com.beebee.chatsystemsendbird.services.auth.login

import android.content.Context
import android.util.Log
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.services.auth.AuthService
import com.beebee.chatsystemsendbird.utils.saveToken
import com.beebee.chatsystemsendbird.utils.saveUserId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class LoginService @Inject constructor(): ILogin {
    override fun loginService(activity: Context, user: User, callback: (String) -> Unit) {
        AuthService.create().loginUser(user)
            .enqueue(object : Callback<User> {
                override fun onFailure(call: Call<User>, t: Throwable) {
                    Log.d("Failed Login", t.localizedMessage)
                    callback("Failed Login")
                }

                override fun onResponse(call: Call<User>, response: Response<User>) {
                    if(response.code() == 401){
                        callback("Failed Login ${response.message()}")
                        return
                    }
                    Log.d("Success Login", response.body().toString())
                    saveToken(activity, response.body()!!.token)
                    saveUserId(activity, response.body()!!._id)
                    callback("Successfully Login")
                }
            })
    }
}
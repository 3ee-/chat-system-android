package com.beebee.chatsystemsendbird.services.chat

import com.beebee.chatsystemsendbird.model.User
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ChatServiceInterface {
    @GET("/profile/contact")
    fun getContacts(): Call<List<User>>
}

object ChatService {
    fun create(token: String): ChatServiceInterface {
        val BASE_URL = "http://192.168.0.6:5000"
        val client = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().addInterceptor { chain ->
                val request = chain.request().newBuilder().addHeader("Authorization", token).build()
                chain.proceed(request)
            }.build())
            .baseUrl(BASE_URL)
            .build()
        return client.create(ChatServiceInterface::class.java)
    }
}
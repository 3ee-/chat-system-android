package com.beebee.chatsystemsendbird.services.auth.register

import com.beebee.chatsystemsendbird.model.User

interface IRegsiter {
    fun registerUser(user: User, callback: (String) -> Unit): Unit
}
package com.beebee.chatsystemsendbird.services.chat.contact

import android.content.Context
import com.beebee.chatsystemsendbird.model.User

interface IContact {
    fun getContacts(activity: Context, callback: (List<User>) -> Unit): Unit
}
package com.beebee.chatsystemsendbird.services.chat.chat_list

import android.content.Context
import com.beebee.chatsystemsendbird.model.Chat

interface IChatList {
    fun getChatList(context: Context, callback: (chatList: List<Chat>) -> Unit)
}
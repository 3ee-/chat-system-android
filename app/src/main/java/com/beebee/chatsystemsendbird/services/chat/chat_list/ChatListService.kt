package com.beebee.chatsystemsendbird.services.chat.chat_list

import android.content.Context
import android.util.Log
import com.beebee.chatsystemsendbird.model.Chat
import com.sendbird.android.GroupChannel
import com.sendbird.android.GroupChannelListQuery
import com.sendbird.android.SendBirdException
import javax.inject.Inject

class ChatListService @Inject constructor(): IChatList {
    override fun getChatList(context: Context, callback: (chatList: List<Chat>) -> Unit) {
        val channel = GroupChannel.createMyGroupChannelListQuery()
        channel.order = GroupChannelListQuery.Order.LATEST_LAST_MESSAGE

        channel.next(object : GroupChannelListQuery.GroupChannelListQueryResultHandler {
            override fun onResult(chat: MutableList<GroupChannel>?, e: SendBirdException?) {
                if (e != null) {
                    Log.d("Chat list error", e.localizedMessage)
                    return
                }
                Log.d("Chat list number", chat?.size.toString())

                val chatList = mutableListOf<Chat>()

                Log.d("Chat list", chat.toString())

                chat?.forEach { response ->
                    chatList.add(
                        Chat(
                            response.lastMessage.message,
                            response.url
                        )
                    )
                }

                callback(chatList)
            }
        })
    }
}
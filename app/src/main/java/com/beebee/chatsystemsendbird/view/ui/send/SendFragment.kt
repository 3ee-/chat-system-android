package com.beebee.chatsystemsendbird.view.ui.send

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.view.custom_view.send.SendContainer
import com.beebee.chatsystemsendbird.viewmodel.send.SendViewModel

class SendFragment : Fragment() {
    private lateinit var contentView: SendContainer
    private val viewModel: SendViewModel
        get() = ViewModelProvider(this).get(SendViewModel::class.java)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_send, container, false).apply {
            contentView = this as SendContainer
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initContent()
    }

    private fun initContent() {
        contentView.initView(viewModel, viewLifecycleOwner)
    }
}
package com.beebee.chatsystemsendbird.view.custom_view.detail

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.beebee.chatsystemsendbird.model.Chat
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.view.ui.detail.DetailAdapter
import com.sendbird.android.*
import kotlinx.android.synthetic.main.fragment_detail.view.*

class DetailContainer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
    private lateinit var adapter: DetailAdapter
    private lateinit var groupChannel: GroupChannel
    private lateinit var listChat: MutableList<Chat>

    fun initView(groupChannelUrl: String) {
        adapter = DetailAdapter()
        listChats.layoutManager = LinearLayoutManager(context)
        listChats.adapter = adapter

        createChannel(groupChannelUrl)

        chat_box.setEndIconOnClickListener {
            sendMessage()
        }
    }

    private fun createChannel(groupChannelUrl: String) {
        GroupChannel.getChannel(groupChannelUrl, object : GroupChannel.GroupChannelGetHandler {
            override fun onResult(groupChannelResponse: GroupChannel, e: SendBirdException?) {
                if (e != null) {
                    return
                }

                groupChannel = groupChannelResponse

                getChats(groupChannel)
            }
        })
    }

    private fun getChats(groupChannel: GroupChannel) {
        listChat = mutableListOf()
        val previousMessageListQuery = groupChannel.createPreviousMessageListQuery()
        previousMessageListQuery.setIncludeMetaArray(true)

        previousMessageListQuery.load(object :  PreviousMessageListQuery.MessageListQueryResult {
            override fun onResult(messageList: MutableList<BaseMessage>?, e: SendBirdException?) {
                if (e != null) {
                    return
                }

                messageList?.forEach { message ->
                    Log.d("Message list", message.toString())
                    listChat.add(
                        Chat(
                            message.message,
                            message.sender.userId
                        )
                    )
                    updateList(listChat)
                }
            }
        })
    }

    private fun sendMessage() {
        val message = chat_box.editText?.text.toString()
        if (message.isNullOrEmpty()) return

        val params = UserMessageParams()
            .setMessage(message)
            .setMentionType(BaseMessageParams.MentionType.CHANNEL)

        groupChannel.sendUserMessage(params, object: BaseChannel.SendUserMessageHandler {
            override fun onSent(message: UserMessage?, e: SendBirdException?) {
                if (e != null) {
                    return
                }
                Log.d("Message send", message?.requestId.toString())
                Log.d("Message send", message?.message.toString())
                chat_box.editText?.setText("")
                listChat.add(
                    Chat(
                        message!!.message,
                        message.requestId
                    )
                )
                updateList(listChat)
            }
        })
    }

    private fun updateList(listChat: List<Chat>) {
        adapter.updateList(listChat)
    }
}
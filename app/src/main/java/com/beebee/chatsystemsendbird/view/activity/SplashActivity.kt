package com.beebee.chatsystemsendbird.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.utils.getToken

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        checkAuth()
    }

    private fun checkAuth() {
        val isToken = getToken(this)
        if (isToken.isNullOrEmpty()) {
            val intent = Intent(this@SplashActivity, AuthActivity::class.java)
            Handler().postDelayed({
                startActivity(intent)
                finish()
            }, 2000)
        } else {
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            Handler().postDelayed({
                startActivity(intent)
                finish()
            }, 2000)
        }
    }
}
package com.beebee.chatsystemsendbird.view.custom_view.send

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.view.ui.send.SendAdapter
import com.beebee.chatsystemsendbird.viewmodel.send.SendViewModel
import kotlinx.android.synthetic.main.fragment_send.view.*

class SendContainer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
    private lateinit var sendAdapter: SendAdapter

    fun initView(viewModel: SendViewModel, lifecycleOwner: LifecycleOwner) {
        sendAdapter = SendAdapter()
        contactList.layoutManager = LinearLayoutManager(context)
        contactList.adapter = sendAdapter

        viewModel.getContacts(context)
        observeViewModel(viewModel, lifecycleOwner)
    }

    private fun observeViewModel(viewModel: SendViewModel, lifecycleOwner: LifecycleOwner) {
        viewModel.contactList.observe(lifecycleOwner, Observer { response ->
            updateList(response)
        })
    }

    private fun updateList(contacts: List<User>) {
        sendAdapter.updateList(contacts)
    }
}
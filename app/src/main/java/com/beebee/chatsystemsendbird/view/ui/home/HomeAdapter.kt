package com.beebee.chatsystemsendbird.view.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.model.Chat
import com.beebee.chatsystemsendbird.view.custom_view.home.HomeListContainer
import kotlinx.android.synthetic.main.item_contact.view.*

class HomeAdapter(
    val chatList: MutableList<Chat> = mutableListOf()
): RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        fun bind(chat: Chat) {
            (view as HomeListContainer).apply {
                initView(chat)
            }
        }
    }

    fun updateList(listChat: List<Chat>) {
        chatList.clear()
        chatList.addAll(listChat)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = chatList.size

    override fun onBindViewHolder(holder: HomeAdapter.ViewHolder, position: Int) {
        holder.bind(chatList[position])
    }
}
package com.beebee.chatsystemsendbird.view.custom_view.home

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.beebee.chatsystemsendbird.model.Chat
import com.beebee.chatsystemsendbird.view.ui.home.HomeAdapter
import com.beebee.chatsystemsendbird.view.ui.home.HomeFragmentDirections
import com.beebee.chatsystemsendbird.viewmodel.home.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeContainer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
    private lateinit var adapter: HomeAdapter

    fun initView(viewModel: HomeViewModel, lifecycleOwner: LifecycleOwner) {
        floating_action_button.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToSendFragment()
            findNavController().navigate(action)
        }
        initContent()
        Handler().postDelayed({
            viewModel.getChat(context)
        }, 1000)
        observeViewModel(viewModel, lifecycleOwner)
    }

    private fun initContent() {
        adapter = HomeAdapter()
        chat_history.layoutManager = LinearLayoutManager(context)
        chat_history.adapter = adapter
    }

    private fun observeViewModel(viewModel: HomeViewModel, lifecycleOwner: LifecycleOwner) {
        viewModel.listChat.observe(lifecycleOwner, Observer { response ->
            updateLists(response)
        })
    }

    private fun updateLists(chatList: List<Chat>) {
        adapter.updateList(chatList)
    }
}
package com.beebee.chatsystemsendbird.view.custom_view.send

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.navigation.findNavController
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.utils.getUserId
import com.beebee.chatsystemsendbird.view.ui.send.SendFragmentDirections
import com.sendbird.android.GroupChannel
import com.sendbird.android.SendBirdException
import kotlinx.android.synthetic.main.item_contact.view.*

class SendItemContainer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
    fun initView(contact: User) {
        emailText.text = contact.email

        emailText.setOnClickListener { goToDetailChat(contact) }
    }

    private fun goToDetailChat(user: User) {
        val userId = getUserId(context).toString()
        val userIds = listOf<String>(user._id, userId)
        val isDistinct = true
        val name = "1-1 ${user._id} & $userId"
        GroupChannel.createChannelWithUserIds(userIds, isDistinct, object : GroupChannel.GroupChannelCreateHandler {
            override fun onResult(channel: GroupChannel?, e: SendBirdException?) {
                if (e != null) {    // Error.
                    return;
                }
                val action = SendFragmentDirections.actionSendFragmentToDetailFragment(channel!!.url)
                findNavController().navigate(action)
            }

        })
    }
}
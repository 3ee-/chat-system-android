package com.beebee.chatsystemsendbird.view.custom_view.login

import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.util.Log
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.view.activity.MainActivity
import com.beebee.chatsystemsendbird.view.ui.login.LoginFragmentDirections
import com.beebee.chatsystemsendbird.viewmodel.login.LoginViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.android.synthetic.main.fragment_signup.view.*
import kotlinx.android.synthetic.main.input_box_layout.view.*

class LoginContainer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
    fun initView(viewModel: LoginViewModel, lifecycleOwner: LifecycleOwner) {
        go_to_signup.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragmentToSignupFragment()
            findNavController().navigate(action)
        }

        login_button.setOnClickListener { loginAction(viewModel, lifecycleOwner) }
        observeViewModel(viewModel, lifecycleOwner)
    }

    private fun loginAction(viewModel: LoginViewModel, lifecycleOwner: LifecycleOwner) {
        val passwordValue = password_box.editText?.text.toString()
        val emailValue = email_box.editText?.text.toString()

        Log.d("Login", "$passwordValue $emailValue")

        val user = User(
            email = emailValue,
            password = passwordValue,
            fullname = "",
            _id = "",
            token = ""
        )

        viewModel.loginCall(context, user)

        login_button.isClickable = false
    }

    private fun observeViewModel(viewModel: LoginViewModel, lifecycleOwner: LifecycleOwner) {
        viewModel._responseLiveData.observe(lifecycleOwner, Observer { response ->
            Log.d("Login Response", "$response")
            login_button.isClickable = true

            Snackbar.make(this, response, Snackbar.LENGTH_LONG)
                .setAction("Close") { }
                .setBackgroundTint(resources.getColor(R.color.colorPrimary))
                .setTextColor(resources.getColor(R.color.colorAccent))
                .setActionTextColor(resources.getColor(R.color.colorPrimaryDark))
                .show()

           if (response == "Successfully Login") {
               password_box.editText?.setText("")
               email_box.editText?.setText("")

               val intent = Intent(context, MainActivity::class.java)
               context.startActivity(intent)
           }
        })
    }
}
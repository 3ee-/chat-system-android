package com.beebee.chatsystemsendbird.view.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.utils.getUserId
import com.sendbird.android.SendBird
import com.sendbird.android.SendBirdException
import com.sendbird.android.User

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initSendbird()
        connectSendbird()
    }

    private fun initSendbird() {
        val APP_ID = "SEND_BIRD_APP_KEY"
        SendBird.init(APP_ID, this)
    }

    private fun connectSendbird() {
        val userId = getUserId(this).toString()
        SendBird.connect(userId, object : SendBird.ConnectHandler {
            override fun onConnected(user: User?, e: SendBirdException?) {
                if (e != null) return
            }
        })
    }
}
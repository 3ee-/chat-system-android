package com.beebee.chatsystemsendbird.view.custom_view.detail

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.beebee.chatsystemsendbird.model.Chat
import kotlinx.android.synthetic.main.item_chat.view.*

class DetailItemContainer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
    fun initView(chat: Chat) {
        chatText.text = chat.messages
    }
}
package com.beebee.chatsystemsendbird.view.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.view.custom_view.home.HomeContainer
import com.beebee.chatsystemsendbird.viewmodel.home.HomeViewModel

class HomeFragment : Fragment() {
    private lateinit var contentView: HomeContainer
    private val viewModel: HomeViewModel
        get() = ViewModelProvider(this).get(HomeViewModel::class.java)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false).apply {
            contentView = this as HomeContainer
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initContent()
    }

    private fun initContent() {
        contentView.initView(viewModel, viewLifecycleOwner)
    }
}
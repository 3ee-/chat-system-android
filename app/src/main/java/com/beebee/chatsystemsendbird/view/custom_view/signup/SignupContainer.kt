package com.beebee.chatsystemsendbird.view.custom_view.signup

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.findNavController
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.viewmodel.signup.SignupViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_signup.view.*
import kotlinx.android.synthetic.main.input_box_layout.view.*
import java.util.*

class SignupContainer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
    fun initView(viewModel: SignupViewModel, observer: LifecycleOwner) {
        go_to_login.setOnClickListener {
            findNavController().popBackStack()
        }
        observeViewModel(viewModel, observer)
        sign_up_button.setOnClickListener { registerUser(viewModel, observer) }
    }

    private fun registerUser(viewModel: SignupViewModel, observer: LifecycleOwner) {
        val passwordValue = password_box.editText?.text.toString()
        val emailValue = email_box.editText?.text.toString()

        val user = User(
            email = emailValue,
            password = passwordValue,
            fullname = "",
            _id = "",
            token = ""
        )

        sign_up_button.isClickable = false

        viewModel.registerUser(user)
    }

    private fun observeViewModel(viewModel: SignupViewModel, observer: LifecycleOwner) {
        viewModel.responseLiveData.observe(observer, androidx.lifecycle.Observer { response ->
            sign_up_button.isClickable = true

            Snackbar.make(this, response, Snackbar.LENGTH_LONG)
                .setAction("Close") { }
                .setBackgroundTint(resources.getColor(R.color.colorPrimary))
                .setTextColor(resources.getColor(R.color.colorAccent))
                .setActionTextColor(resources.getColor(R.color.colorPrimaryDark))
                .show()

            password_box.editText?.setText("")
            email_box.editText?.setText("")
        })
    }
}
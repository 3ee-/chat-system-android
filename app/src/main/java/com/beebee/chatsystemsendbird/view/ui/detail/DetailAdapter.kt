package com.beebee.chatsystemsendbird.view.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.model.Chat
import com.beebee.chatsystemsendbird.view.custom_view.detail.DetailItemContainer

class DetailAdapter(
    val chatList: MutableList<Chat> = mutableListOf()
) : RecyclerView.Adapter<DetailAdapter.ViewHolder>() {
    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        fun bind(chat: Chat) {
            (view as DetailItemContainer).apply {
                initView(chat)
            }
        }
    }

    fun updateList(newChatList: List<Chat>) {
        chatList.clear()
        chatList.addAll(newChatList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = chatList.size

    override fun onBindViewHolder(holder: DetailAdapter.ViewHolder, position: Int) {
        holder.bind(chatList[position])
    }

}
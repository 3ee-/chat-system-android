package com.beebee.chatsystemsendbird.view.ui.signup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.view.custom_view.signup.SignupContainer
import com.beebee.chatsystemsendbird.viewmodel.signup.SignupViewModel

class SignupFragment : Fragment() {
    private lateinit var contentView: SignupContainer
    private val viewModel: SignupViewModel
        get() = ViewModelProvider(this).get(SignupViewModel::class.java)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signup, container, false).apply {
            contentView = this as SignupContainer
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.renderContent()
    }


    private fun renderContent() {
        contentView.initView(viewModel, viewLifecycleOwner)
    }
}
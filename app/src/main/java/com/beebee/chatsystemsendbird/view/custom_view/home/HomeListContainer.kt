package com.beebee.chatsystemsendbird.view.custom_view.home

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.beebee.chatsystemsendbird.model.Chat
import com.beebee.chatsystemsendbird.view.ui.home.HomeFragmentDirections
import kotlinx.android.synthetic.main.item_chat_history.view.*
import kotlinx.android.synthetic.main.item_contact.view.*
import kotlinx.android.synthetic.main.item_contact.view.emailText

class HomeListContainer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {
    fun initView(chat: Chat) {
        emailText.text = chat.messages

        item_chat_history.setOnClickListener { goToDetail(chat) }
    }

    private fun goToDetail(chat: Chat) {
        val url = chat.userId
        val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(url)
        findNavController().navigate(action)
    }
}
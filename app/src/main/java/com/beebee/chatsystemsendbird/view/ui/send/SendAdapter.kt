package com.beebee.chatsystemsendbird.view.ui.send

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.view.custom_view.send.SendItemContainer

class SendAdapter(
    val contacts: MutableList<User> = mutableListOf()
) : RecyclerView.Adapter<SendAdapter.ViewHolder>() {
    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        fun bind(contact: User) {
            (view as SendItemContainer).apply {
                initView(contact)
            }
        }
    }

    fun updateList(newList: List<User>) {
        contacts.clear()
        contacts.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SendAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = contacts.size

    override fun onBindViewHolder(holder: SendAdapter.ViewHolder, position: Int) {
        holder.bind(contacts[position])
    }
}
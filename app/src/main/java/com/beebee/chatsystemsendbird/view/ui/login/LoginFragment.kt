package com.beebee.chatsystemsendbird.view.ui.login

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.view.custom_view.login.LoginContainer
import com.beebee.chatsystemsendbird.viewmodel.login.LoginViewModel

class LoginFragment : Fragment() {
    private lateinit var contentView: LoginContainer
    private val viewModel: LoginViewModel
        get() = ViewModelProvider(this).get(LoginViewModel::class.java)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false).apply {
            contentView = this as LoginContainer
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.initContent()
    }

    private fun initContent() {
        contentView.initView(viewModel, viewLifecycleOwner)
    }
}
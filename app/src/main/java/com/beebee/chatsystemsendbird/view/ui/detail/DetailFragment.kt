package com.beebee.chatsystemsendbird.view.ui.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.beebee.chatsystemsendbird.R
import com.beebee.chatsystemsendbird.view.custom_view.detail.DetailContainer

class DetailFragment : Fragment() {
    private lateinit var contentView: DetailContainer
    private val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false).apply {
            contentView = this as DetailContainer
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        renderContent()
    }

    private fun renderContent() {
        contentView.initView(args.groupChannel)
    }
}
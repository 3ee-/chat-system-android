package com.beebee.chatsystemsendbird.viewmodel.send

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.services.chat.contact.ContactService
import toothpick.Toothpick
import javax.inject.Inject

class SendViewModel : ViewModel() {
    @Inject
    lateinit var contactService: ContactService

    private val _contactList = MutableLiveData<List<User>>()
    val contactList = _contactList

    init {
        val scope = Toothpick.openScope(this)
        Toothpick.inject(this, scope)
    }

    fun getContacts(activity: Context) {
        contactService.getContacts(activity) { response ->
            _contactList.postValue(response)
        }
    }
}
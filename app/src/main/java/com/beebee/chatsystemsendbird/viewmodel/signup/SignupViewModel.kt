package com.beebee.chatsystemsendbird.viewmodel.signup

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.services.auth.register.RegisterService
import toothpick.Toothpick
import javax.inject.Inject

class SignupViewModel: ViewModel() {
    @Inject
    lateinit var registerService: RegisterService
    private val _responseLiveData = MutableLiveData<String>()
    val responseLiveData = _responseLiveData

    init {
        val scope = Toothpick.openScope(this)
        Toothpick.inject(this, scope)
    }

    fun registerUser(user: User) {
        registerService.registerUser(user) {value ->
            _responseLiveData.postValue(value)
        }
    }
}
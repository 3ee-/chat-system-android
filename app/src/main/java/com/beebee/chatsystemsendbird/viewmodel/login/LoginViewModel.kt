package com.beebee.chatsystemsendbird.viewmodel.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beebee.chatsystemsendbird.model.User
import com.beebee.chatsystemsendbird.services.auth.login.LoginService
import toothpick.Toothpick
import javax.inject.Inject

class LoginViewModel : ViewModel() {
    @Inject
    lateinit var loginService: LoginService

    private  val responseLiveData = MutableLiveData<String>()
    val _responseLiveData = responseLiveData

    init {
        val scope = Toothpick.openScope(this)
        Toothpick.inject(this, scope)
    }

    fun loginCall(activity: Context, user: User) {
        loginService.loginService(activity, user) { response ->
            responseLiveData.postValue(response)
        }
    }
}
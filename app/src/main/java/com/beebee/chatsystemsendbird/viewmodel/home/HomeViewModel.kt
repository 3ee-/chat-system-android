package com.beebee.chatsystemsendbird.viewmodel.home

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beebee.chatsystemsendbird.model.Chat
import com.beebee.chatsystemsendbird.services.chat.chat_list.ChatListService
import toothpick.Toothpick
import javax.inject.Inject

class HomeViewModel : ViewModel() {
    @Inject
    lateinit var chatService: ChatListService
    private val _listChat = MutableLiveData<List<Chat>>()
    val listChat = _listChat

    init {
        val scope = Toothpick.openScope(this)
        Toothpick.inject(this, scope)
    }

    fun getChat(context: Context) {
        Log.d("Get chat viewmodel", "hello")
        chatService.getChatList(context) { response ->
            _listChat.postValue(response)
        }
    }
}
package com.beebee.chatsystemsendbird.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val _id: String,
    val fullname: String?,
    val email: String,
    val password: String,
    val token: String
): Parcelable
package com.beebee.chatsystemsendbird.model

data class Chat (
    val messages: String,
    val userId: String
)